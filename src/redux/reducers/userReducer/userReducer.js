import actionsType from "../../actions/actionType"

const initState = {
    users: [],
}

export const userReducer = (state = initState, action) => {
    switch (action.type) {
        case actionsType.getAllUsers: {
            const listUsers = action.payLoad.listUsers;

            return { users: [...state.users, listUsers] }
        }

        case actionsType.addUser: {
            const user = action.payLoad.user;
            
            return { users: [...state.users, user] }
        }
        default:
            return state
    }
}

export default userReducer;