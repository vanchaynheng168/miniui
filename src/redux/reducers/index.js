import { combineReducers } from 'redux'
import postReducer from './postReducer/postReducer';
import userReducer from './userReducer/userReducer';

const reducers = {
    postReducer: postReducer,
    userReducer: userReducer
};

export const rootReducer = combineReducers(reducers);
