import React, { Component } from 'react'
import { Form, Button,Row, Container,Col,Image } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { addPost } from "../../../redux/actions/postActions";

export class CreatePost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            title: "",
            body: "",
        }
    }
    
    onChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        const newPost = {}; 
        newPost.title = this.state.title;
        newPost.body = this.state.body;
        this.props.addPost(newPost);
        this.setState({ loading: true, title: '', body: ''});
        setTimeout(() => {
            alert("Add post successfully!");
            this.setState({ loading: false });
        }, 5000);
    }

    render() {
        const { loading, title, body } = this.state;
        if(loading) {
            return <h1>Loading...</h1>;
        }
        return (
            <Container className="py-3">
                <Link to='/posts'>GoBack</Link>
                <h1>Add Article</h1>
                <div>
                    <Row>
                        <Col md={7}>
                        <Form onSubmit={this.onSubmit}>
                            <Form.Group>
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" 
                                    placeholder="Enter title" 
                                    name="title" 
                                    onChange={this.onChange}
                                    value={title}/>
                            </Form.Group>
                            <Container>
                                <Row>
                                    <p className="mr-5">Category :</p>
                                        {[ 'radio'].map((type) => (
                                            <div key={`custom-inline-${type}`} className="mb-3">
                                            <Form.Check
                                                custom
                                                inline
                                                label="Action Movies"
                                                type={type}
                                                name='select'
                                                id={`custom-inline-${type}-1`}
                                            />
                                            <Form.Check
                                                custom
                                                inline
                                                label="Sport"
                                                name='select'
                                                type={type}
                                                id={`custom-inline-${type}-2`}
                                            />
                                            <Form.Check
                                                custom
                                                inline
                                                label="News"
                                                name='select'
                                                type={type}
                                                id={`custom-inline-${type}-3`}
                                            />
                                            </div>
                                        ))}
                                </Row>
                            </Container>
                            
                            <Form.Group>
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" 
                                    placeholder="Enter title" 
                                    name="title" 
                                    onChange={this.onChange}
                                    value={title}/>
                            </Form.Group>
                            <Form>
                            <Form.File 
                                id="custom-file"
                                label="Custom file input"
                                custom
                                className = "mb-3"
                            />
                            </Form>
                            <Button variant="primary" type="submit">Submit</Button>
                        </Form>
                        </Col>
                        <Col md={5}>
                            <Image src="https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png" rounded  style={{width: '353px', height: '243px'}}/>
                        </Col>
                    </Row>
                </div>
                
            </Container>
        )
    }
}

CreatePost.propTypes = {
    addPost: PropTypes.func.isRequired
}

export default connect(null, { addPost })(CreatePost);

