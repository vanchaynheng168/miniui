import React, { Component } from 'react'
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getPostById, updatePost } from "../../../redux/actions/postActions";

export class UpdatePost extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            id: null,
            title: "",
            body: "",
            loading: true,
        }
    }
    
    componentDidMount() {
        const { match, getPostById } = this.props;
        const id = match.params.id;
        getPostById(id);       
    }

    componentDidUpdate() {
        const { current, loading } = this.props.post;
        if(this.state.id !== current.id) {
            this.setState({ id: current.id, title: current.title, body: current.body, loading: loading });
        }
    }

    onChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        const newPost = {}; 
        newPost.id = this.state.id;
        newPost.title = this.state.title;
        newPost.body = this.state.body;
        this.props.updatePost(newPost);
        this.setState({ loading: true });

        setTimeout(() => {
            alert("Update post successfully!");
            this.setState({ title: '', body: '', loading: false});
            this.props.history.push('/posts');
        }, 5000);

    }

    render() {
        const { title, body, loading } = this.state;
        if(loading) {
            return <h1>Loading...</h1>;
        }
        return (
            <div className="py-3">
                <h1>Update Post</h1>
                <Form>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter title" 
                            name="title" 
                            onChange={this.onChange}
                            value={title}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Body</Form.Label>
                        <Form.Control as="textarea" 
                            rows="3" 
                            name="body" 
                            onChange={this.onChange} 
                            placeholder="Enter body"
                            value={body}/>
                    </Form.Group>
                    <Button className="mr-1" variant="primary" type="submit" onClick={this.onSubmit}>Update</Button>
                    <Button variant="dark" type="submit" onClick={() => this.props.history.push('/posts')}>Cancel</Button>
                </Form>
            </div>
        )
    }
}

UpdatePost.propTypes = {
    post: PropTypes.object.isRequired,
    getPostById: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    post: state.postReducer,
});

export default connect(mapStateToProps, { getPostById, updatePost })(UpdatePost);

