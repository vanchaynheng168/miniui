import React, { useState } from 'react'
import { Button, Container, Image,Row,Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deletePost } from '../../../redux/actions/postActions';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner'


const PostItem = ({ post, deletePost }) => {
    const [loading, setLoading] = useState(false);
    const onDelete = () => {
        deletePost(post.id);
        setLoading(true);
        setTimeout(() => {
            alert('Delete post successfully!');
            setLoading(false);
        }, 5000);
    }
    // if(loading) {
    //     return <div>
    //         {/* <h2>Loadingqew....</h2> */}
    //         <Spinner animation="border" />
    //         </div>
    // }
    return (
        <div>
            <Container>
                <Row>
                    <Col xs={5} md={4} className="mb-3">
                    <Image src="https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png" rounded  style={{width: '353px', height: '243px'}}/>
                    </Col>
                    <Col xs={7} md={8}>
                        <h2> Bootstrap at its core</h2>
                        <p>Create Date : 2020-5-24</p>
                        <p className= "mb-4">Category : Sport</p>
                        <p>Before get up please miss me pg ,good night see you tommorw, Before get up please miss me pg ,good night see you tommorw</p>
                        <Button variant="dark" onClick={() => alert(`id: ${post.id} title: ${post.title}`)}>View</Button>
                        <Button className="mx-1" variant="warning" as={Link} to={`/posts/update/${post.id}`}>Update</Button>
                        <Button variant="danger" onClick={onDelete}>Delete</Button>
                    </Col>
                </Row> 
            </Container>
            {/* <div>
                <Button variant="dark" onClick={() => alert(`id: ${post.id} title: ${post.title}`)}>View</Button>
                <Button className="mx-1" variant="warning" as={Link} to={`/posts/update/${post.id}`}>Update</Button>
                <Button variant="danger" onClick={onDelete}>Delete</Button>
            </div> */}
        </div>
    )
}

PostItem.propTypes = {
    post: PropTypes.object.isRequired,
    deletePost: PropTypes.func.isRequired,
}

export default connect(null, { deletePost })(PostItem);
