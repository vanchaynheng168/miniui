import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PostItem  from './PostItem';
import PropTypes from 'prop-types';
import { getAllPosts } from "../../../redux/actions/postActions";
import { BoxLoading } from 'react-loadingg';
import { Row, Col,Button,FormControl,Form, Container, } from 'react-bootstrap';

class ListPosts extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            loading: true,
            posts: null,  
        }
    }

    componentDidMount() {
        const { getAllPosts } = this.props;
        getAllPosts();
    }

    componentDidUpdate() {
        const { posts, loading } = this.props.post;
        if(this.state.posts !== posts) {
            this.setState({ posts, loading });
        }
    }

    render() {
        const { loading, posts } = this.state;
        
        if(loading || posts === null) {
            return  <div style={{textAlign: "center"}}>
                <BoxLoading />;
            </div>
        }
        return (
            <div className="py-3">
                <Link to="/posts/create">Create Post</Link>
                <div className="mt-4 mb-5">
                <Row>
                    <Col xs={5} md={5}>
                    <h1>Article Management</h1>
                    </Col>
                    <Col xs={7} md={7}>
                    <Form inline>
                    <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>Category </Form.Label>
                        <Form.Control as="select" custom className="ml-2 mr-2">
                        <option>All</option>
                        <option>Sport</option>
                        <option>Security</option>
                        </Form.Control>
                    </Form.Group>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-success"className="mr-2">Search</Button>
                            <Button variant="primary">Add Article</Button>
                        </Form>
                    </Col>
                </Row>
            </div>
                { !loading && posts.length === 0 ? (<h3>No data yet...</h3>
                ) : (
                    posts.map(post => <PostItem post={post} key={post.id}/>)
                )}
            </div>
        )
    }
}

ListPosts.propTypes = {
    post: PropTypes.object.isRequired,
    getAllPosts: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    post: state.postReducer
});

export default connect(mapStateToProps, { getAllPosts })(ListPosts);
