import React, { Component } from 'react'
import { Table,Form, Container ,Button,Row} from 'react-bootstrap';

export default class Category extends Component {
    render() {
        return (
            <div>
                <h1>Category</h1>

                
                    <Form>
                    <Row>
                    <Form.Group controlId="formGroupEmail" className="mt-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" style={{width : "200px"}} />
                    </Form.Group>
                    <div style={{marginTop: '48px'}} className="ml-2">
                    <Button variant="primary" className="mr-2">Add Article</Button>
                    </div>
                    </Row>
                    </Form>
                    
               
      
                
           
                <Table style={{textAlign: 'center'}} striped bordered hover>
                    <thead >
                        <tr >
                        <th style={{width: '180px'}}>#</th>
                        <th style={{width: '700px'}}>Name</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>1</td>
                        <td>Mark</td>
                        <td><Button variant="primary" className="mr-2">Edit</Button><Button variant="danger">Danger</Button></td>
                        </tr>
                    </tbody>
                </Table>
       
            </div>
        )
    }
}
