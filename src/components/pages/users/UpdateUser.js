import React, { Component } from 'react'
import Axios from "axios";
import { Form, Button } from "react-bootstrap";

export default class UpdateUser extends Component {
    state = {
        id: 0,
        email: "",
        loading: true,
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        Axios.get(`https://reqres.in/api/users/${id}`, { timeout: 5000 })
            .then(res => {
                if(res.status === 200) {
                    return res.data.data;
                }
            })
            .then(data => this.setState({ loading: false, id, email: data.email }))
            .catch(err => console.log(err));        
    }

    onChange = (event) => {
        const value = event.target.value;
        this.setState({ email: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading : true});
        
        const data = {};
            data.email = this.state.email;
            Axios.put(`https://reqres.in/api/users/${this.state.id}`, data, 
                { timeout: 5000, headers: { 'Content-Type': 'application/json' }, data: JSON.stringify(data) })
                .then(res => {
                    if(res.status === 200) {
                        alert("Update user successfully!");
                        this.setState({ loading: false });
                        return res.data;
                    }
                })
                .then(data => console.log(data))
                .catch(err => {
                    if(err.response) {
                        if(err.response.status === 404) {
                            alert("404 Page, Not Found!");
                            this.setState({ loading: false });
                        }
                        else {
                            alert("Create post failed!'");
                            this.setState({ loading: false });
                        }
                    }
                    console.log(err);
                    this.setState({ loading: false });
                });
    }

    render() {
        if(this.state.loading) {
            return <h1>Loading...</h1>
        }
        return (
            <div className="py-3">
                 <h1>Update User</h1>
                 <Form onSubmit={this.onSubmit}>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" name="email" value={this.state.email} onChange={this.onChange}/>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>
                    <Button className="ml-1" variant="dark" type="submit">Cancel</Button>
                </Form>
            </div>
        )
    }
}
