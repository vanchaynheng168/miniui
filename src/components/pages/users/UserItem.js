import React from 'react';
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import Axios from 'axios';

export const UserItem = ({ user }) => { 
    return (
        <div>
            <h1>{user.id}</h1>
            <h3>{user.email}</h3>
            <div>
                <Button variant="dark" onClick={() => alert(`id: ${user.id} email: ${user.email}`)}>View</Button>
                <Button className="mx-1" variant="warning" as={Link} to={`/users/update/${user.id}`}>Update</Button>
                <Button variant="danger" onClick={() => {
                    Axios.delete(`https://reqres.in/api/users/${user.id}`, { timeout: 5000 })
                    .then(res => {
                        if(res.status === 204) {
                            alert("Delete user successfully!");
                        }
                    })
                    .catch(err => {
                        if(err.response) {
                            if(err.response.status === 404) {
                                alert("404 Page, Not Found!");
                            }
                            else {
                                alert("Create post failed!'");
                            }
                        }
                        console.log(err);
                    });
                }}>Delete</Button>
            </div>
        </div>
    )
}
