import React, { Component } from 'react';
import { Form, Button } from "react-bootstrap";
import Axios from 'axios';

export default class CreateUser extends Component {
    state = {
        email: "",
        loading: false,
    }
    
    onChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading : true});
        
        const data = {}; 
        data.email = this.state.email;
        data.job = this.state.job;

        Axios.post("https://reqres.in/api/users", data, 
            { timeout: 5000, headers: { 'Content-Type': 'application/json' }, data: JSON.stringify(data) })
                .then(res => { 
                    if(res.status === 201) {
                        alert("Create user successfully!");
                        this.setState({ loading: false });
                        return res.data;
                    }
                })
                .then(data => console.log(data))
                .catch(err => {
                    if(err.response) {
                        if(err.response.status === 404) {
                            alert("404 Page, Not Found!");
                            this.setState({ loading: false });
                        }
                        else {
                            alert("Create post failed!'");
                            this.setState({ loading: false });
                        }
                    }
                    console.log(err);
                    this.setState({ loading: false });
                });
    }

    render() {
        if(this.state.loading) {
            return <h1>Loading...</h1>
        }
        return (
            <div className="py-3">
                 <h1>Create User</h1>
                 <Form onSubmit={this.onSubmit}>
                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" name="email" value={this.state.email} onChange={this.onChange}/>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>
                </Form>
            </div>
        )
    }
}
