import React, { Component } from 'react'
import Axios from "axios";
import { Link } from "react-router-dom";
import { UserItem } from './UserItem';

export default class ListUsers extends Component {
    state = {
        loading: true,
        users: [],
    }
    componentDidMount() {
        // fetch("https://reqres.in/api/users?page=2")
        //     .then(res => res.json())
        //     .then(result => this.setState({ loading: false, users: result.data}))
        //     .catch(err => console.log(err));

        Axios.get("https://reqres.in/api/users?page=2", { timeout: 5000 })
            .then(res => this.setState({ loading: false, users: res.data.data}))
            .catch(err => {
                if(err.response) { 
                    if (err.response.status === 404) {
                        alert('Error: Page Not Found');
                        this.setState({ loading: false });
                    }
                    else if (err.request) {
                        // Request was made but no response
                        console.error(err.request);
                        this.setState({ loading: false });
                    } 
                    else {
                        console.error(err.message);
                        this.setState({ loading: false });
                    }
                }
                console.log(err);
                this.setState({ loading: false });
            });
    }
    render() {
        const { loading, users } = this.state;
        if(loading) {
            return <h1>Loading...</h1>
        }
        return (
            <div className="py-3">
                <Link to="/users/create">Create User</Link>
                { users.length === 0 && <h3>No data yet...</h3>}
                {users.length > 0 && users.map(user => <UserItem user={user} key={user.id}/>)}
            </div>
        )
    }
}
